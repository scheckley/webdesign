function showCode(parentElement) {
    // Get the parent element by its ID
    var parentElementDiv = document.getElementById(parentElement);

    // Find the example element within the parent element using querySelector
    var exampleElement = document.querySelector('#' + parentElement + ' .example');

    // Check if the parent element has the class 'open'
    if (parentElementDiv.className == 'section open') {
        // search for the example code block and assign it to the codeBlock variable
        var codeBlock = exampleElement.querySelector('.example-code');
        // remove the code block
        exampleElement.removeChild(codeBlock);
        //reset the button back to show code
        document.querySelector('#' + parentElement + ' .code-show-button').value = "Show code";
        // remove the div class from the parent element
        parentElementDiv.classList.remove('open');
    } else {
        // If the parent element is not open, retrieve the HTML content of the example element
        var text = exampleElement.innerHTML;

        // Create a text node with the retrieved HTML content
        var code = document.createTextNode(text);

        // Create a new div element to hold the code text node
        var codeElement = document.createElement('div');

        // Append the code text node to the code div element
        codeElement.appendChild(code);

        // Set the class of the code div element to 'example-code'
        codeElement.className = 'example-code';

        // Append the code div element to the example element
        exampleElement.appendChild(codeElement);

        // Change the value of the code-show-button to "Hide code"
        document.querySelector('#' + parentElement + ' .code-show-button').value = "Hide code";

        // Add the 'open' class to the parent element to mark it as open
        parentElementDiv.classList.add('open');
    }
}

